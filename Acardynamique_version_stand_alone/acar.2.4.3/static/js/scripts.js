

function maj_style_bouton(elementId, accomplieValue) {
    var buttonElement = document.getElementById(elementId);

    if (buttonElement) {
        buttonElement.classList.remove('accomplie-0', 'accomplie-1','accomplie-2');
        buttonElement.classList.add('accomplie-' + accomplieValue);
    }
}


function maj_sabliers(sabliers,donnees) {
    Object.entries(sabliers).forEach(function([id, sablier]) {
        var id_valeur_sablier = "id_valeur_sablier" + id;
        var id_bouton_sablier = "id_bouton_sablier" + id;
        var id_bouton_sablier_restant = "id_bouton_sablier_restant" + id;
        var duree_restante = sablier['duree_restante'];
        var valeur_sablier_restant = "valeur_sablier_restant" + id;
        

        var valeurSablierElement = document.getElementById(id_valeur_sablier);
        var buttonSablierElement = document.getElementById(id_bouton_sablier);
        var buttonSablierRestantElement = document.getElementById(id_bouton_sablier_restant);

        if (valeurSablierElement) {
            valeurSablierElement.innerText = sablier.duree_restante;
            }

        if (buttonSablierRestantElement ) {
            buttonSablierRestantElement.innerText = sablier.repetition_restante;
            }
            
        if (buttonSablierElement) {
            accomplie = donnees[id]['accomplie'];

            maj_style_bouton(id_bouton_sablier, accomplie);
            }

        if (buttonSablierRestantElement) {
            accomplie = donnees[id]['accomplie'];

            maj_style_bouton(id_bouton_sablier_restant, accomplie);
            }

        if (duree_restante === '00:00:00' && sablier['repetition_restante']>= 0 && sablier['isRunning']==='true'&& sablier['sound'])  {
            
            audios["audio_sablier" + id].play();
            
            $(document).ready(function () {
            
                const alerte = '<div id="alerte'+sablier.repetition_restante+'" class="alert alert-success" role="alert">'+donnees[id]["label"]+'</div>';
                // Ajouter l'alerte au corps de la page
                $('body').append(alerte);
                    $('#alerte'+sablier.repetition_restante).on('click', function () {
                        $(this).alert('close'); // Cette méthode ferme l'alerte Bootstrap
                    });
                });
            }
    });
}




function maj_chronos(chronos, donnees) {
 
    Object.entries(chronos).forEach(function([id, chrono]) {
        var id_valeur_chrono = "id_valeur_chrono" + id;
        var id_bouton_chrono = "id_bouton_chrono" + id;

        var valeurChronoElement = document.getElementById(id_valeur_chrono);
        var buttonElementChrono = document.getElementById(id_bouton_chrono);

        if (valeurChronoElement) {
            valeurChronoElement.innerText = chrono.temps_ecoule;
        }

        if (buttonElementChrono) {
            var accomplieValue = donnees[id].accomplie;
            maj_style_bouton(id_bouton_chrono, accomplieValue);
        }
    });
}

function maj_taches(donnees) {
    Object.entries(donnees).forEach(function([id, tache]) {
        var accomplieValue = tache.accomplie;
        maj_style_bouton('id_bouton_tache' + id, accomplieValue);
    });
}



function maj_tache_bouton(id, accomplieValue) {
    maj_style_bouton('id_bouton_tache' + id, accomplieValue);
}

function maj_tache_sablier_valeur(id, duree_restante) {
    var id_valeur_sablier = "id_valeur_sablier" + id;
    var valeurSablierElement = document.getElementById(id_valeur_sablier);

    if (valeurSablierElement) {
        valeurSablierElement.innerText = duree_restante;
    }
}

function maj_repetition_sablier_valeur(id, valeur_repetition) {
    var id_repetition_sablier = "repetition_sablier" + id;
    var valeurRepetitionSablierElement = document.getElementById(id_repetition_sablier);

    if (valeurRepetitionSablierElement) {
        valeurRepetitionSablierElement.innerText = valeur_repetition;
    }
}


function maj_tache_chrono_valeur(id, temps) {
    var id_valeur_chrono = "id_valeur_chrono" + id;
    var valeurChronoElement = document.getElementById(id_valeur_chrono);

    if (valeurChronoElement) {
        valeurChronoElement.innerText = temps;
    }
}




function maj(id_acar,slide) {
    fetch('/maj/id_acar='+id_acar+'/slide='+slide)
        .then(response => {
            if (!response.ok) {
                throw new Error('Réponse du serveur non OK');
            }
            return response.json();
        })
        .then(data => {
            var sabliers = data.sabliers;
            var chronos = data.chronos;
            var donnees = data.donnees;
          
            maj_sabliers(sabliers,donnees);
            
            maj_chronos(chronos, donnees);
            maj_taches(donnees);
        })
        .catch(error => {
            console.error('Erreur lors de la récupération des données JSON:', error);
        });
}

function playAudio(audioElement) {
    return new Promise((resolve) => {
        audioElement.onended = resolve;
        audioElement.play();
    });
}

async function modifier(event, tache_id, id_acar, slide) {
    event.preventDefault();

    try {
        const response = await fetch('/modifier/tache_id=' + tache_id + '/id_acar=' + id_acar + '/slide=' + slide, { method: 'POST' });
        const data = await response.json();

        var donnees = data.donnees;
        var sabliers = data.sabliers;
        var chronos = data.chronos;
        var repetition_sablier = "repetition_sablier" + tache_id;

        var accomplieValue = donnees[tache_id].accomplie;
        maj_tache_bouton(tache_id, accomplieValue);

        if (donnees[tache_id]['sound']) {
            await playAudio(audios["audio_tache" + tache_id]);
        }

        if (tache_id in sabliers) {
            var sablier = sabliers[tache_id];
            var duree_restante = sablier.duree_restante;

            if (sablier.isRunning === "true") {
                maj_tache_sablier_valeur(tache_id, sablier.temps_restant);
                maj_repetition_sablier_valeur(tache_id, sablier.repetition_restante);
            }
        }

        if (tache_id in chronos) {
            var chrono = chronos[tache_id];
            if (chrono.pause === "false") {
                maj_tache_chrono_valeur(tache_id, chrono.temps);
            }
        }

        maj(id_acar, slide);
    } catch (error) {
        console.error('Erreur lors de la requête fetch :', error);
    }
}


    

function reset_acar(event, id_acar ) {
    event.preventDefault();
    fetch('/reset_acar/id_acar='+id_acar, {method: 'POST'})
    .then(response => response.json())
    .then(data => {
        

            // Parcourez la liste de dictionnaires pour trouver celui avec l'ID correspondant
            var donnees = data.donnees;
            var sabliers = data.sabliers;
            var chronos = data.chronos;
            var id_slide = data.id_slide;
            window.location.href = `/acar/id_acar=${id_acar}/slide=1`;

        })

    .catch(error => {
        console.error('Erreur lors de la requête fetch :', error);
    });
}

function decision(id_acar, slide) {
    fetch(`/decision/id_acar=${id_acar}/slide=${slide}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Réponse du serveur non OK');
            }
            return response.text(); 
        })
        .then(data => {
            

            try {
                const jsonData = JSON.parse(data);
                var slide_destination = jsonData.slide_destination;
                if (slide_destination !== 0) {
                    window.location.href = `/acar/id_acar=${id_acar}/slide=${slide_destination}`;
                }
            } catch (error) {
                console.error('Erreur lors de l\'analyse JSON :', error);
            }
        })
        .catch(error => {
            console.error('Erreur lors de la récupération des données JSON :', error);
        });
}
