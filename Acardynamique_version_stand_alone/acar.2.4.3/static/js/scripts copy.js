function maj_style_bouton(elementId, accomplieValue) {
    var buttonElement = document.getElementById(elementId);

    if (buttonElement) {
        buttonElement.classList.remove('accomplie-0', 'accomplie-1');
        buttonElement.classList.add('accomplie-' + accomplieValue);
    }
}


function maj_sabliers(sabliers) {
    Object.entries(sabliers).forEach(function([id, sablier]) {
        var id_valeur_sablier = "id_valeur_sablier" + id;
        var id_bouton_sablier = "id_bouton_sablier" + id;

        var valeurSablierElement = document.getElementById(id_valeur_sablier);
        var buttonElementSablier = document.getElementById(id_bouton_sablier);

        if (valeurSablierElement) {
            valeurSablierElement.innerText = sablier.temps_restant;
        }

        if (buttonElementSablier) {
            var temps_restant = sablier.temps_restant;
            maj_style_bouton(id_bouton_sablier, temps_restant > 0 ? 1 : 0);
        }
    });
}


function maj_chronos(chronos, donnees) {
    Object.entries(chronos).forEach(function([id, chrono]) {
        var id_valeur_chrono = "id_valeur_chrono" + id;
        var id_bouton_chrono = "id_bouton_chrono" + id;

        var valeurChronoElement = document.getElementById(id_valeur_chrono);
        var buttonElementChrono = document.getElementById(id_bouton_chrono);

        if (valeurChronoElement) {
            valeurChronoElement.innerText = chrono.temps;
        }

        if (buttonElementChrono) {
            var accomplieValue = donnees[id].accomplie;
            maj_style_bouton(id_bouton_chrono, accomplieValue);
        }
    });
}

function maj_taches(donnees) {
    Object.entries(donnees).forEach(function([id, tache]) {
        var accomplieValue = tache.accomplie;
        maj_style_bouton('id_bouton_tache' + id, accomplieValue);
    });
}


function maj(id_slide) {
    fetch('/maj')
        .then(response => response.json())
        .then(data => {
            var sabliers = data.sabliers;
            var chronos = data.chronos;
            var donnees = data.donnees;

            maj_sabliers(sabliers);
            maj_chronos(chronos, donnees);
            maj_taches(donnees);
        });
}


function maj(id_slide) {
    //Mise à jour des données , des sabliers et des chronos .
    // maj est appelée dans la  page toutes les secondes

    fetch('/maj')  
    .then(response => response.json())
    .then(data => {
        // réception des données 
       
        var donnees = data.donnees;
        var sabliers = data.sabliers;
        var chronos = data.chronos;

        // parcours des sabliers pour mettre à jours les données de temps et des  css associés
        
        Object.entries(sabliers).forEach(function([id, sablier]) {
            // mise à jour des valeurs des sabliers dans la page
            id_valeur_sablier = "id_valeur_sablier"+id ;   
            if (document.getElementById(id_valeur_sablier)) {
                    document.getElementById(id_valeur_sablier).innerText = sablier.temps_restant ;
            }

            // mise à jour du css du bouton sablier
            id_bouton_sablier = "id_bouton_sablier"+id ;
            if (document.getElementById(id_bouton_sablier)) {
                var buttonElementSablier = document.getElementById(id_bouton_sablier);  
                var temps_restant = sablier.temps_restant ;
                if (temps_restant> 0){
                
                // Mise à jour de la classe CSS du bouton
                buttonElementSablier.classList.remove('accomplie-0', 'accomplie-1');
                buttonElementSablier.classList.add('accomplie-1');
                    }
                else {
                        // Mise à jour de la classe CSS du bouton
                buttonElementSablier.classList.remove('accomplie-0', 'accomplie-1');
                buttonElementSablier.classList.add('accomplie-0');
                }

                }
         
            
         });
         
           
            

        // parcours des chronos pour mettre à jours les données de temps et des  css associés
        Object.entries(chronos).forEach(function([id, chrono]) {
                

                    id_valeur_chrono = "id_valeur_chrono"+id;   
                    if (document.getElementById(id_valeur_chrono)) {
                    
                        document.getElementById(id_valeur_chrono).innerText = chrono.temps ;
                    }

                    id_bouton_chrono = "id_bouton_chrono"+id;
                    
                        
                    if (document.getElementById(id_bouton_chrono)) {
                        var buttonElementChrono = document.getElementById(id_bouton_chrono);  
                        var temps = chrono.temps;
                        var accomplieValue = data.donnees[id].accomplie;
                        if (buttonElementChrono !== null  && accomplieValue) {
                    
                            // Mise à jour de la classe CSS du bouton
                            buttonElementChrono.classList.remove('accomplie-0', 'accomplie-1');
                            buttonElementChrono.classList.add('accomplie-' + accomplieValue);
                        }
                    }
        });
            
        
                    
        
        
        // mise à jour du css des boutons taches 
        Object.entries(donnees).forEach(function([id, tache]) {
            
            var accomplieValue = data.donnees[id].accomplie;
            var buttonElement = document.getElementById('id_bouton_tache'+id);
  
            if (buttonElement!== null){
                // Mise à jour de la classe CSS du bouton
                buttonElement.classList.remove('accomplie-0', 'accomplie-1');
                buttonElement.classList.add('accomplie-' + accomplieValue);
            }
            
        });

    });


}

    

function reset_acar(event, id_acar ) {
    event.preventDefault();
    fetch('/reset_acar/' + id_acar, {method: 'POST'})
    .then(response => response.json())
    .then(data => {
        

        // Parcourez la liste de dictionnaires pour trouver celui avec l'ID correspondant
        var donnees = data.donnees;
        var sabliers = data.sabliers;
        var chronos = data.chronos;
        maj(id_slide);
    });
    .catch(error => {
        console.error('Erreur lors de la requête fetch :', error);
    });
}


function modifier(event, tache_id, id_slide) {
    event.preventDefault();
    fetch('/modifier/' + tache_id, {method: 'POST'})
    .then(response => response.json())
    .then(data => {
        

        // Parcourez la liste de dictionnaires pour trouver celui avec l'ID correspondant
        var donnees = data.donnees;
        var sabliers = data.sabliers;
        var chronos = data.chronos;

        // Assurez-vous que l'ID correspond à un élément dans le DOM
        var buttonElement = document.getElementById('id_bouton_tache'+tache_id);
        var accomplieValue = donnees[tache_id].accomplie;
        
        if (buttonElement && accomplieValue !== undefined) {
                
                // Mise à jour de la classe CSS du bouton
                buttonElement.classList.remove('accomplie-0', 'accomplie-1');
                buttonElement.classList.add('accomplie-' + accomplieValue);
        }

        
        if (sabliers[tache_id].isRunning === "true") {
                    
            if (document.getElementById("id_valeur_sablier"+tache_id)) {
                document.getElementById("id_valeur_sablier"+tache_id).innerText = sabliers[tache_id].temps_restant ;
            }
        }
          
        if (chronos[id].pause === "false") {
                        
            if (document.getElementById("id_valeur_chrono"+tache_id) {
            document.getElementById("id_valeur_chrono"+tache_id).innerText = chronos[tache_id].temps ;
            }function maj(id_slide) {
                //Mise à jour des données , des sabliers et des chronos .
                // maj est appelée dans la  page toutes les secondes
            
                fetch('/maj')  
                .then(response => response.json())
                .then(data => {
                    // réception des données 
                   
                    var donnees = data.donnees;
                    var sabliers = data.sabliers;
                    var chronos = data.chronos;
            
                    // parcours des sabliers pour mettre à jours les données de temps et des  css associés
                    
                    Object.entries(sabliers).forEach(function([id, sablier]) {
                        // mise à jour des valeurs des sabliers dans la page
                        id_valeur_sablier = "id_valeur_sablier"+id ;   
                        if (document.getElementById(id_valeur_sablier)) {
                                document.getElementById(id_valeur_sablier).innerText = sablier.temps_restant ;
                        }
            
                        // mise à jour du css du bouton sablier
                        id_bouton_sablier = "id_bouton_sablier"+id ;
                        if (document.getElementById(id_bouton_sablier)) {
                            var buttonElementSablier = document.getElementById(id_bouton_sablier);  
                            var temps_restant = sablier.temps_restant ;
                            if (temps_restant> 0){
                            
                            // Mise à jour de la classe CSS du bouton
                            buttonElementSablier.classList.remove('accomplie-0', 'accomplie-1');
                            buttonElementSablier.classList.add('accomplie-1');
                                }
                            else {
                                    // Mise à jour de la classe CSS du bouton
                            buttonElementSablier.classList.remove('accomplie-0', 'accomplie-1');
                            buttonElementSablier.classList.add('accomplie-0');
                            }
            
                            }
                     
                        
                     });
                     
                       
                        
            
                    // parcours des chronos pour mettre à jours les données de temps et des  css associés
                    Object.entries(chronos).forEach(function([id, chrono]) {
                            
            
                                id_valeur_chrono = "id_valeur_chrono"+id;   
                                if (document.getElementById(id_valeur_chrono)) {
                                
                                    document.getElementById(id_valeur_chrono).innerText = chrono.temps ;
                                }
            
                                id_bouton_chrono = "id_bouton_chrono"+id;
                                
                                    
                                if (document.getElementById(id_bouton_chrono)) {
                                    var buttonElementChrono = document.getElementById(id_bouton_chrono);  
                                    var temps = chrono.temps;
                                    var accomplieValue = data.donnees[id].accomplie;
                                    if (buttonElementChrono !== null  && accomplieValue) {
                                
                                        // Mise à jour de la classe CSS du bouton
                                        buttonElementChrono.classList.remove('accomplie-0', 'accomplie-1');
                                        buttonElementChrono.classList.add('accomplie-' + accomplieValue);
                                    }
                                }
                    });
                        
                    
                                
                    
                    
                    // mise à jour du css des boutons taches 
                    Object.entries(donnees).forEach(function([id, tache]) {
                        
                        var accomplieValue = data.donnees[id].accomplie;
                        var buttonElement = document.getElementById('id_bouton_tache'+id);
              
                        if (buttonElement!== null){
                            // Mise à jour de la classe CSS du bouton
                            buttonElement.classList.remove('accomplie-0', 'accomplie-1');
                            buttonElement.classList.add('accomplie-' + accomplieValue);
                        }
                        
                    });
            
                });
            
            
            }
            
                
            
            function reset_acar(event, id_acar ) {
                event.preventDefault();
                fetch('/reset_acar/' + id_acar, {method: 'POST'})
                .then(response => response.json())
                .then(data => {
                    
            
                    // Parcourez la liste de dictionnaires pour trouver celui avec l'ID correspondant
                    var donnees = data.donnees;
                    var sabliers = data.sabliers;
                    var chronos = data.chronos;
                    maj(id_slide);
                });
                .catch(error => {
                    console.error('Erreur lors de la requête fetch :', error);
                });
            }
            
            
            function modifier(event, tache_id, id_slide) {
                event.preventDefault();
                fetch('/modifier/' + tache_id, {method: 'POST'})
                .then(response => response.json())
                .then(data => {
                    
            
                    // Parcourez la liste de dictionnaires pour trouver celui avec l'ID correspondant
                    var donnees = data.donnees;
                    var sabliers = data.sabliers;
                    var chronos = data.chronos;
            
                    // Assurez-vous que l'ID correspond à un élément dans le DOM
                    var buttonElement = document.getElementById('id_bouton_tache'+tache_id);
                    var accomplieValue = donnees[tache_id].accomplie;
                    
                    if (buttonElement && accomplieValue !== undefined) {
                            
                            // Mise à jour de la classe CSS du bouton
                            buttonElement.classList.remove('accomplie-0', 'accomplie-1');
                            buttonElement.classList.add('accomplie-' + accomplieValue);
                    }
            
                    
                    if (sabliers[tache_id].isRunning === "true") {
                                
                        if (document.getElementById("id_valeur_sablier"+tache_id)) {
                            document.getElementById("id_valeur_sablier"+tache_id).innerText = sabliers[tache_id].temps_restant ;
                        }
                    }
                      
                    if (chronos[id].pause === "false") {
                                    
                        if (document.getElementById("id_valeur_chrono"+tache_id) {
                        document.getElementById("id_valeur_chrono"+tache_id).innerText = chronos[tache_id].temps ;
                        }
                    }
                    
                    maj(id_slide);
                });
            
                    
                
                .catch(error => {
                    console.error('Erreur lors de la requête fetch :', error);
                });
            }
            
            
            function updateChrono_general() {
                    fetch('/maj_chrono_general')  
                        .then(response => response.json())
                        .then(data => {
                            if (document.getElementById('valeur_chrono_general')) {
                            document.getElementById('valeur_chrono_general').innerText = data.valeur_chrono_general ;
                            }
                        });
                }
        }
        
        maj(id_slide);
    });

        
    
    .catch(error => {
        console.error('Erreur lors de la requête fetch :', error);
    });
}


function updateChrono_general() {
        fetch('/maj_chrono_general')  
            .then(response => response.json())
            .then(data => {
                if (document.getElementById('valeur_chrono_general')) {
                document.getElementById('valeur_chrono_general').innerText = data.valeur_chrono_general ;
                }
            });
    }