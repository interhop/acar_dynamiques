# Récupérer le chemin du répertoire principal du projet
import os,sys,time,time
from flask import current_app
project_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(project_path)

from fonctions.chrono import *
from copy import deepcopy


titre_acar3 = "ARRET CARDIAQUE AU BLOC OPERATOIRE"

id_acar3 = 3

taches_acar3 = {
        1: {'id':1,   'label':'Absence de pouls',                                         'slide':1,'dateheure':'', 'accomplie': 0},
        2: {'id':2,   'label':'Effondrement de la capnie',                                'slide':1,'dateheure':'', 'accomplie': 0},
        3: {'id':3,   'label':'Appel à l’aide',                                           'slide':2,'dateheure':'', 'accomplie': 0},
        4: {'id':4,   'label':'STOP CHIRURGIE',                                           'slide':2,'dateheure':'', 'accomplie': 0},
        5: {'id':5,   'label':'Si non débuté Commencer massage cardiaque',                'slide':2,'commentaire': "120 /minutes , 5 a 6 cm de dépression thoracique ,EtCO2>10mmHg , Temps de compression= temps de relâchement",'dateheure':'', 'accomplie': 0},
        6: {'id':6,   'label':'IOT ou masque laryngé',                                    'slide':3,'dateheure':'', 'accomplie': 0},
        7: {'id':7,   'label':'Couper gaz anesthésie',                                    'slide':3,'dateheure':'', 'accomplie': 0},
        8: {'id':8,   'label':'Mettre Fi O2 100 % fr 12 débit 15 L/min.',                 'slide':3,'dateheure':'', 'accomplie': 0},
        9: {'id':9,   'label':'KTIO si pas de VVP',                                       'slide':3,'dateheure':'', 'accomplie': 0},
        10: {'id':10, 'label':'Faire venir DSA',                                        'slide':4,'dateheure':'', 'accomplie': 0},
        11: {'id':11, 'label':"Sortir l’ Adrénaline",                                   'slide':4,'dateheure':'', 'accomplie': 0},
        12: {'id':12, 'label':"Faire venir l’amiodarone ( 3 ampoules)",                 'slide':4,'dateheure':'', 'accomplie': 0},
        13: {'id':13, 'label':'Mettre en place DSA',                                    'slide':5,'dateheure':'', 'accomplie': 0},
        14: {'id':14, 'label':'Mettre en place DAE',                                    'slide':5,'dateheure':'', 'accomplie': 0},
        15: {'id':15, 'label':'Choc 1 / défibrillation +Reprise massage',               'slide':6,"sablier":1,'dateheure':'', 'accomplie': 0},
        16: {'id':16, 'label':'reprise en sinusal',                                     'slide':6,'dateheure':'', 'accomplie': 0},
        17: {'id':17, 'label':"Choc 2 / défibrillation +Reprise massage",               'slide':7,"sablier":1,'dateheure':'', 'accomplie': 0},
        18: {'id':18, 'label':'reprise en sinusal',                                     'slide':7,'dateheure':'', 'accomplie': 0},
        19: {'id':19, 'label':'Choc 3 / défibrillation +Reprise massage',               'slide':8,"sablier":1,'dateheure':'', 'accomplie': 0},
        20: {'id':20, 'label':'Si pas de reprise en sinusal injecter 1 mg d’adrénaline','slide':8,'dateheure':'', 'accomplie': 0},
        21: {'id':21, 'label':"300mg d’amiodarone",                                     'slide':8,'dateheure':'', 'accomplie': 0},
        22: {'id':22, 'label':'reprise en sinusal',                                     'slide':8,'dateheure':'', 'accomplie': 0},
        23: {'id':23, 'label':'Choc 4 / défibrillation +Reprise massage',               'slide':9,"sablier":1,'dateheure':'', 'accomplie': 0},
        24: {'id':24, 'label':'reprise en sinusal',                                     'slide':9,'dateheure':'', 'accomplie': 0},
        25: {'id':25, 'label':'Choc 5 / défibrillation +Reprise massage',               'slide':10,"sablier":1,'dateheure':'', 'accomplie': 0},
        26: {'id':26, 'label':'Si pas de reprise en sinusal injecter 1 mg d’adrénaline','slide':10,'dateheure':'', 'accomplie': 0},
        27: {'id':27, 'label':"150 mg d’amiodarone",                                    'slide':10,'dateheure':'', 'accomplie': 0},
        28: {'id':28, 'label':'reprise en sinusal',                                     'slide':10,'dateheure':'', 'accomplie': 0},
        29: {'id':29, 'label':'Choc 6 / défibrillation +Reprise massage',               'slide':11,"sablier":1,'dateheure':'', 'accomplie': 0},
        30: {'id':30, 'label':'reprise en sinusal',                                     'slide':11,'dateheure':'', 'accomplie': 0},
        31: {'id':31, 'label':'Choc 7 / défibrillation +Reprise massage',               'slide':12,"sablier":1,'dateheure':'', 'accomplie': 0},
        32: {'id':32, 'label':'Si pas de reprise en sinusal injecter 1 mg d’adrénaline','slide':12,'dateheure':'', 'accomplie': 0},
        33: {'id':33, 'label':'900 mg d’amiodarone/jr IVSE',                            'slide':12,'dateheure':'', 'accomplie': 0},
        34: {'id':34, 'label':'reprise en sinusal',                                     'slide':12,'dateheure':'', 'accomplie': 0},
        35: {'id':35, 'label':'PAS d’indication de Choc',                               'slide':13,'dateheure':'', 'accomplie': 0},
        36: {'id':36, 'label':'Reprise massage',                                        'slide':13,'dateheure':'', 'accomplie': 0},
        37: {'id':37, 'label':"PAS d’indication de Choc",                               'slide':14,'dateheure':'', 'accomplie': 0},
        38: {'id':38, 'label':"Injection de 1 mg d’adrénaline",                         'slide':14,'dateheure':'', 'accomplie': 0},
        39: {'id':39, 'label':'Adrénaline',                                             'slide':14,"sablier":1,'dateheure':'', 'accomplie': 0},
        40: {'id':39, 'label':'Fin',                                                    'slide':15,'dateheure':'', 'accomplie': 0}
            }

sabliers_acar3 ={
            15 : {'id' : 15 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            17 : {'id' : 17 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            19 : {'id' : 19 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            23 : {'id' : 23 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            25 : {'id' : 25 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            29 : {'id' : 29 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            31 : {'id' : 31 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' },
            39 : {'id' : 38 ,'repetition':0,'repetition_restante': 0, 'duree' : 120, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false' }
              }

chronos_acar3 ={
            0 : {'id' : 0 , 'temps_depart': 0 , 'temps':0, 'temps_ecoule' : 0 , 'pause':'true' , 'bouton_pause': 'true' },

                }
decisions_acar3 = {
            1 : [{'activation' : [1,2], 'slide_destination' : 2  }] ,
            2 : [{'activation' : [3,4,5], 'slide_destination' : 3}] ,
            3 : [{'activation' : [6,7,8,9], 'slide_destination' : 4} ],
            4 : [{'activation' : [10,11,12], 'slide_destination' : 5}] ,
            5 : [{'activation' : [13], 'slide_destination' : 6},{'activation' : [14], 'slide_destination' :11 }] ,
            6 : [{'activation' : [15], 'slide_destination' : 7},{'activation' : [16], 'slide_destination' : 15}] ,
            7 : [{'activation' : [17], 'slide_destination' : 8},{'activation' : [18], 'slide_destination' : 15}] ,
            8 : [{'activation' : [19,20,21], 'slide_destination' : 9},{'activation' : [22], 'slide_destination' : 15}] ,
            9 : [{'activation' : [23], 'slide_destination' : 10},{'activation' : [24], 'slide_destination' : 15}] ,
            10 : [{'activation' : [25,26,27,28], 'slide_destination' : 15}] ,
            11 : [{'activation' : [29], 'slide_destination' : 12},{'activation' : [30], 'slide_destination' : 15}] ,
            12 : [{'activation' : [31,32,33], 'slide_destination' : 13},{'activation' : [34], 'slide_destination' : 15}] ,
            13 : [{'activation' : [35,36], 'slide_destination' : 14}] ,
            14 : [{'activation' : [37,38,39], 'slide_destination' : 15}] 

                }

