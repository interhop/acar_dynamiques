# Récupérer le chemin du répertoire principal du projet
import os,sys,time,time
from flask import current_app
project_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(project_path)

from fonctions.chrono import *
from copy import deepcopy


id_acar1 = 1

titre_acar1 = "Premier test d'Acar"

taches_acar1 = {
         1 : {'id': 1 , 'label':'Absence de pouls','slide':1,'dateheure':'','sound':'beep.mp3' ,'accomplie': 0},
         2 : {'id': 2 , 'label':'Effondrement de la capnie','slide':1 ,'dateheure':'', 'accomplie': 0},
         3 : {'id': 3 , 'label':'Commencer massage cardiaque','slide':2 , "sablier":1,  'dateheure':'', 'accomplie': 0},
         4 : {'id': 4 , 'label':'IOT ou masque laryngé','slide':3 ,'chrono' :1, 'dateheure':'', 'accomplie': 0},
         5 : {'id': 5 , 'label':'Mettre Fi O2 100 %','slide':3 ,"sablier":1, 'dateheure':'', 'accomplie': 0},
         6 : {'id': 6 , 'label':'Couper gaz anesthésie','slide':3 ,'chrono' :1, 'dateheure':'', 'accomplie': 0},
         7 : {'id': 7 , 'label':' KTIO si pas de VVP','slide':3 , 'dateheure':'', 'accomplie': 0},
         8 : {'id': 8 , 'label':' Faire venir DSA','slide':4 , 'dateheure':'', 'accomplie': 0},
         9 : {'id': 9 , 'label':' Sortir l’ Adrénaline','slide':4 , 'dateheure':'', 'accomplie': 0},
        10 : {'id': 10 , 'label':' Faire venir l’amiodarone ( 3 ampoules)','slide':4 , 'dateheure':'', 'accomplie': 0},
        11 : {'id': 11 , 'label':' Mettre en place DSA / DAE','slide':4 , 'dateheure':'', 'accomplie': 0}
        
            }

sabliers_acar1 ={
            3 : {'id' : 3 ,'repetition':3,'repetition_restante': 0, 'duree' : 10, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false', 'sound':'beep.mp3' },
            5 : {'id' : 5 , 'repetition': 0,'repetition_restante': 0 , 'duree' : 10, 'duree_restante':0,'temps_depart':0,'temps':0,'isRunning':'false'}
                }

chronos_acar1 ={
            0 : {'id' : 0 , 'temps_depart': 0 , 'temps':0, 'temps_ecoule' : 0 , 'pause':'true' , 'bouton_pause': 'true' },
            4 : {'id' : 4 , 'temps_depart': 0 , 'temps':0 , 'temps_ecoule' : 0 , 'pause':'true' , 'bouton_pause': 'true' },
            6 : {'id' : 6 , 'temps_depart': 0 , 'temps':0 , 'temps_ecoule' : 0 , 'pause':'true' , 'bouton_pause': 'true' },
                }
decisions_acar1 = {
            1 : [{'activation' : [1], 'slide_destination' : 2  },{'activation' : [2], 'slide_destination' : 3  } ],
            2 : [ {'activation' : [3], 'slide_destination' : 3} ]
                }

