import time
from datetime import datetime , timedelta


def convertir_secondes_en_hms(secondes):
    # Créer un objet timedelta avec le nombre de secondes
    delta = timedelta(seconds=secondes)

    # Utiliser l'objet timedelta pour calculer les heures, minutes et secondes
    heures, reste = divmod(delta.seconds, 3600)
    minutes, secondes = divmod(reste, 60)

    # Formater le résultat comme HH:MM:SS
    temps_formate = "{:02}:{:02}:{:02}".format(heures, minutes, secondes)

    return temps_formate



class chrono :
    def __init__(self):
        self.datetime = 0
        self.start = 0
        self.end = 0
        self.en_pause = True
        self.temps_ecoule = 0
    

    def demarrer( self):
        self.datetime = datetime.today()
        self.start = time.perf_counter()
        self.en_pause  = False
    
    def donner_chrono(self):
        if self.en_pause == True:
            return self.temps_ecoule
        else :
           
            self.temps_ecoule = self.temps_ecoule + time.perf_counter() - self.start 
            self.start = time.perf_counter()
            return self.temps_ecoule

    def pause (self):
        if self.en_pause == True :
            self.en_pause = False
            self.start = time.perf_counter()
        elif self.en_pause  == False :
            self.en_pause  = True
            self.temps_ecoule =self.temps_ecoule + time.perf_counter() - self.start 
            

        


class sablier :
    def __init__(self, temps):
        self.temps_initial = temps
        self.temps = temps
        self.started = False
        self.datetime = 0
        self.start = 0
        self.end = 0
        self.temps_restant = temps


    def demarrer( self):
        if not self.started :
            self.datetime = datetime.today()
            self.start = time.perf_counter()
        self.started = True

    def donner_temps_restant(self):
        if self.started : 
            self.temps_restant = self.temps - int(time.perf_counter() - self.start)
            if self.temps_restant < 0 :
                self.temps_restant = 0
                self.started = False
        return self.temps_restant
    
    def redemarrer(self) : 
        self.temps = self.temps_initial
        self.started = False
        self.datetime = 0
        self.start = 0
        self.end = 0
        self.temps_restant = self.temps