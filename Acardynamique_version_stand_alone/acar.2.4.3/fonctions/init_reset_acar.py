from flask import Blueprint, Flask, render_template, request, redirect, url_for, current_app, flash,jsonify
from copy import deepcopy

from donnees_acars.donnees_acar1 import *
from donnees_acars.donnees_acar3 import *

def init_donnees(id_acar):
    
    if 'acar_'+str(id_acar) not in current_app.config :

      
        current_app.config['acar_'+str(id_acar)] ={}

        current_app.config['acar_'+str(id_acar)]['id_acar'] = eval('id_acar'+str(id_acar))
        current_app.config['acar_'+str(id_acar)]['titre_acar'] = eval('titre_acar'+str(id_acar))
        current_app.config['acar_'+str(id_acar)]['donnees'] = deepcopy(eval('taches_acar'+str(id_acar)))
        current_app.config['acar_'+str(id_acar)]['sabliers'] = deepcopy(eval('sabliers_acar'+str(id_acar)))
        current_app.config['acar_'+str(id_acar)]['chronos'] = deepcopy(eval('chronos_acar'+str(id_acar)))
        current_app.config['acar_'+str(id_acar)]['decisions'] = deepcopy(eval('decisions_acar'+str(id_acar)))
        
    
    


def reset_donnees(id_acar):
    
    current_app.config['acar_'+str(id_acar)] ={}

    current_app.config['acar_'+str(id_acar)]['id_acar'] = eval('id_acar'+str(id_acar))
    current_app.config['acar_'+str(id_acar)]['titre_acar'] = eval('titre_acar'+str(id_acar))
    current_app.config['acar_'+str(id_acar)]['donnees'] = deepcopy(eval('taches_acar'+str(id_acar)))
    current_app.config['acar_'+str(id_acar)]['sabliers'] = deepcopy(eval('sabliers_acar'+str(id_acar)))
    current_app.config['acar_'+str(id_acar)]['chronos'] = deepcopy(eval('chronos_acar'+str(id_acar)))
    current_app.config['acar_'+str(id_acar)]['decisions'] = deepcopy(eval('decisions_acar'+str(id_acar)))
    