from flask import Blueprint, Flask, render_template, request, redirect, url_for, current_app, flash,jsonify
import os,sys,time
from datetime import datetime , timedelta
from copy import deepcopy


# Récupérer le chemin du répertoire principal du projet


project_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(project_path)
from fonctions.chrono import *
from fonctions.init_reset_acar import *
from donnees_acars.donnees_acar1 import *
from donnees_acars.donnees_acar3 import *



acar_bp = Blueprint('acar', __name__)

    


@acar_bp.route("/acar/id_acar=<int:id_acar>/slide=<int:slide>")
def f_acar(id_acar, slide):
    init_donnees(id_acar)

    if current_app.config['acar_'+str(id_acar)]['chronos'][0]['temps_depart'] == 0 :

        current_app.config['acar_'+str(id_acar)]['chronos'][0]['temps_depart'] =  time.perf_counter()
        current_app.config['acar_'+str(id_acar)]['chronos'][0]['pause']='false'

    donnees = current_app.config['acar_'+str(id_acar)]['donnees']
    id_acar = current_app.config['acar_'+str(id_acar)]['id_acar']
    titre_acar = current_app.config['acar_'+str(id_acar)]['titre_acar']
    sabliers= deepcopy(current_app.config['acar_'+str(id_acar)]['sabliers'])
    for id,sablier in sabliers.items():
        sablier['duree_restante']=convertir_secondes_en_hms(sablier['duree_restante'])
    chronos= deepcopy(current_app.config['acar_'+str(id_acar)]['chronos'])
    for id,chrono in chronos.items():
        chrono['temps_ecoule']=convertir_secondes_en_hms(chrono['temps_ecoule'])
    decisions = deepcopy(current_app.config['acar_'+str(id_acar)]['decisions'])
    

    return render_template("acar"+str(id_acar)+"/acar"+str(id_acar)+"_"+str(slide)+".html",
                                            donnees = donnees,
                                            sabliers = sabliers,
                                            chronos =  chronos,
                                            id_acar = id_acar,
                                            titre_acar = titre_acar,
                                            decisions = decisions,
                                            slide = slide
                                            )
                                        

