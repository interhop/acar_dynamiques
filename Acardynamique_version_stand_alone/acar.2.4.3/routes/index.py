from flask import Blueprint, Flask, render_template, request, redirect, url_for, session,flash

main_bp = Blueprint('main', __name__ )



@main_bp.route("/")
def index():
    return render_template("index.html")