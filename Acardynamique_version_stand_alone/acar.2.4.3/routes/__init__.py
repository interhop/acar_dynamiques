# le fichier __init__.py permet l'import des fichiers contenus dans le dossier routes.
# on y indique les emplacements des fichiers que contient le dossier routes


from flask import Blueprint



routes_bp = Blueprint('routes', __name__)

from .index import main_bp
from .acar import acar_bp
from .routes_de_base_acar import acar_base_bp





