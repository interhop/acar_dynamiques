from flask import Blueprint, Flask, render_template, request, redirect, url_for, current_app, flash,jsonify
import time
from datetime import datetime , timedelta
# on importe le contenu du dossier routes 
acar_base_bp = Blueprint('acar_base', __name__)

# Récupérer le chemin du répertoire principal du projet
import os,sys,time,time
from copy import deepcopy
project_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(project_path)


from fonctions.chrono import *
from fonctions.init_reset_acar import *
from donnees_acars.donnees_acar1 import *
from donnees_acars.donnees_acar3 import *

@acar_base_bp.route('/modifier/tache_id=<int:tache_id>/id_acar=<int:id_acar>/slide=<int:slide>', methods=['POST'])
def modifier(tache_id,id_acar,slide):

    tache = current_app.config['acar_'+str(id_acar)]['donnees'][tache_id]
    if not 'sablier' in tache.keys() and not 'chrono' in tache.keys():
        tache['accomplie'] = 1 - tache['accomplie']

    
    if tache_id in current_app.config['acar_'+str(id_acar)]['sabliers'] : 
    
        sablier = current_app.config['acar_'+str(id_acar)]['sabliers'][tache_id]
  
        if  sablier['isRunning'] == 'false' :
            sablier['isRunning']='true'
            current_app.config['acar_'+str(id_acar)]['donnees'][tache_id]['accomplie'] = 2
            sablier['repetition_restante']=sablier['repetition']
            sablier['duree_restante'] = sablier['duree']
            sablier['temps_depart'] = time.perf_counter()
            sablier['duree_restante']=sablier['duree'] - (time.perf_counter() -sablier['temps_depart'])


    if tache_id in current_app.config['acar_'+str(id_acar)]['chronos'] : 

        chrono = current_app.config['acar_'+str(id_acar)]['chronos'][tache_id]
        tache = current_app.config['acar_'+str(id_acar)]['donnees'][tache_id]
        if chrono['pause'] == 'false' :
            chrono['pause']='true'
            tache['accomplie']=0

            
        elif chrono['pause'] == 'true' :
            if chrono['temps_depart'] == 0 :
                chrono['temps_depart'] =  time.perf_counter()
                chrono['pause']='false'
                tache['accomplie']=2
            else : 
                chrono['pause']='false'
                chrono['temps_depart'] =  time.perf_counter()-chrono['temps_ecoule']
                tache['accomplie']=2
               
    # Préparation des données à envoyer             
    donnees = current_app.config['acar_'+str(id_acar)]['donnees']
    id_acar = current_app.config['acar_'+str(id_acar)]['id_acar']
    titre_acar = current_app.config['acar_'+str(id_acar)]['titre_acar']
    sabliers= deepcopy(current_app.config['acar_'+str(id_acar)]['sabliers'])
    for id,sablier in sabliers.items():
        sablier['duree_restante']=convertir_secondes_en_hms(sablier['duree_restante'])
    chronos= deepcopy(current_app.config['acar_'+str(id_acar)]['chronos'])
    for id,chrono in chronos.items():
        chrono['temps_ecoule']=convertir_secondes_en_hms(chrono['temps_ecoule'])
    decisions = deepcopy(current_app.config['acar_'+str(id_acar)]['decisions'])
    response_data = {'donnees': donnees, 
                    'sabliers': sabliers,
                    'chronos': chronos, 
                    'decisions': decisions ,  
                    "slide": slide, 
                    "id_acar": id_acar, 
                    "titre_acar": titre_acar
                    }
  
    return jsonify(response_data)


@acar_base_bp.route('/maj/id_acar=<int:id_acar>/slide=<int:slide>')
def maj(id_acar,slide):
       
    for id , sablier in  current_app.config['acar_'+str(id_acar)]['sabliers'].items():

        if sablier['isRunning']=='true':
            sablier['temps']= time.perf_counter()
            sablier['duree_restante']=sablier['duree'] - (time.perf_counter() -sablier['temps_depart'])


        if sablier['duree_restante'] < 0 : # remise à zéro de la tache sablier lorsque le sablier est terminé
                if sablier['repetition_restante']>0 :
                    sablier['repetition_restante'] -=1
                    sablier['duree_restante'] = sablier['duree']
                    sablier['temps_depart'] = time.perf_counter()
                    sablier['duree_restante']=sablier['duree'] - (time.perf_counter() -sablier['temps_depart'])



                elif sablier['repetition_restante']==0 :

                    tache = current_app.config['acar_'+str(id_acar)]['donnees'][id]
                    tache['accomplie'] = 1 
                    sablier['isRunning'] = 'false'
                    sablier['duree_restante'] = 0

    
    for id, chrono in  current_app.config['acar_'+str(id_acar)]['chronos'].items():
        

        if chrono['pause'] == 'false':
            chrono['temps'] = time.perf_counter()
            chrono['temps_ecoule'] = chrono['temps']-chrono['temps_depart']



    # Préparation des données à envoyer 
    donnees = current_app.config['acar_'+str(id_acar)]['donnees']
    id_acar = current_app.config['acar_'+str(id_acar)]['id_acar']
    titre_acar = current_app.config['acar_'+str(id_acar)]['titre_acar']
    sabliers= deepcopy(current_app.config['acar_'+str(id_acar)]['sabliers'])
    for id,sablier in sabliers.items():
        sablier['duree_restante']=convertir_secondes_en_hms(sablier['duree_restante'])
    chronos= deepcopy(current_app.config['acar_'+str(id_acar)]['chronos'])
    for id,chrono in chronos.items():
        chrono['temps_ecoule']=convertir_secondes_en_hms(chrono['temps_ecoule'])
    decisions = deepcopy(current_app.config['acar_'+str(id_acar)]['decisions'])

    response_data = {'donnees': donnees, 
                    'sabliers': sabliers,
                    'chronos': chronos, 
                    'decisions': decisions ,  
                    "slide": slide, 
                    "id_acar": id_acar, 
                    "titre_acar": titre_acar
                    }

    return jsonify(response_data)




@acar_base_bp.route("/decision/id_acar=<int:id_acar>/slide=<int:slide>")
def decision(id_acar,slide):

    donnees = current_app.config['acar_'+str(id_acar)]['donnees']
    id_acar = current_app.config['acar_'+str(id_acar)]['id_acar']
    titre_acar = current_app.config['acar_'+str(id_acar)]['titre_acar']
    
    sabliers= deepcopy(current_app.config['acar_'+str(id_acar)]['sabliers'])
    for id,sablier in sabliers.items():
        sablier['duree_restante']=convertir_secondes_en_hms(sablier['duree_restante'])

    chronos= deepcopy(current_app.config['acar_'+str(id_acar)]['chronos'])
    for id,chrono in chronos.items():
        chrono['temps_ecoule']=convertir_secondes_en_hms(chrono['temps_ecoule'])
    
    decisions = deepcopy(current_app.config['acar_'+str(id_acar)]['decisions'])


    slide_destination = 0
    if slide in decisions:
        liste_decisions = decisions[slide] # liste de dictionnaires de décision
        

        for decision in liste_decisions:
            activation = decision['activation']
            
            if {tache['accomplie'] for id,tache in donnees.items() if tache['slide']==slide and id in activation} == {1} :

                slide_destination = decision['slide_destination']
        

    # Préparation des données à envoyer 
    response_data = {'donnees': donnees, 
                    'sabliers': sabliers,
                    'chronos': chronos, 
                    'decisions': decisions ,  
                    "slide": slide,
                    "slide_destination" :slide_destination ,  
                    "id_acar": id_acar, 
                    "titre_acar": titre_acar
                    }
    return jsonify(response_data)
                                        

@acar_base_bp.route('/reset_acar/id_acar=<int:id_acar>', methods=['POST'])
def reset_acar(id_acar):
    reset_donnees(id_acar)
    if current_app.config['acar_'+str(id_acar)]['chronos'][0]['temps_depart'] == 0 :
    
        current_app.config['acar_'+str(id_acar)]['chronos'][0]['temps_depart'] =  time.perf_counter()
        current_app.config['acar_'+str(id_acar)]['chronos'][0]['pause']='false'
   

    donnees = current_app.config['acar_'+str(id_acar)]['donnees']
    id_acar = current_app.config['acar_'+str(id_acar)]['id_acar']
    titre_acar = current_app.config['acar_'+str(id_acar)]['titre_acar']
    sabliers= deepcopy(current_app.config['acar_'+str(id_acar)]['sabliers'])
    for id,sablier in sabliers.items():
        sablier['duree_restante']=convertir_secondes_en_hms(sablier['duree_restante'])
    chronos= deepcopy(current_app.config['acar_'+str(id_acar)]['chronos'])
    for id,chrono in chronos.items():

        chrono['temps_ecoule']=convertir_secondes_en_hms(chrono['temps_ecoule'])
    decisions = deepcopy(current_app.config['acar_'+str(id_acar)]['decisions'])
    response_data = {'donnees': donnees, 
                    'sabliers': sabliers,
                    'chronos': chronos, 
                    'decisions': decisions ,  
                    "id_acar": id_acar, 
                    "titre_acar": titre_acar
                    }
    
    
    
    return jsonify(response_data)
   
    
