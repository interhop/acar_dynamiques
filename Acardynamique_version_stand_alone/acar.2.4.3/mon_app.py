from flask import Flask, render_template, request, redirect, url_for
from flask import Blueprint
from fonctions.chrono import *
from routes import routes_bp, main_bp, acar_bp,acar_base_bp
app = Flask(__name__)


app.secret_key = "9d263e09465118fcc3b288369ed53396922588fc3e8f466845ef8ab6a00cef25"

app.config['SESSION_COOKIE_PATH'] = '/'
app.config['SESSION_COOKIE_NAME'] = 'your_session_cookie_name'

# Enregistrez les Blueprints
app.register_blueprint(routes_bp)
app.register_blueprint(main_bp)
app.register_blueprint(acar_bp)
app.register_blueprint(acar_base_bp)


if __name__ == '__main__':
    app.run(host = '127.0.0.1', port = 5000)
    app.add_url_rule('/favicon.ico',redirect_to=url_for('static', filename='favicon.ico'))

