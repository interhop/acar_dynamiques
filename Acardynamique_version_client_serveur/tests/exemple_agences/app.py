from flask import Flask, render_template
import sqlite3

app = Flask(__name__)

@app.route('/')
def index():
    # Connexion à la base de données SQLite
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    
    # Récupération des données des tables véhicule et agence
    c.execute("SELECT * FROM vehicule")
    vehicules = c.fetchall()
    
    c.execute("SELECT * FROM agence")
    agences = c.fetchall()
    c.execute("SELECT * FROM location")
    locations = c.fetchall()
    
    conn.close()
    
    return render_template('index.html', vehicules=vehicules, agences=agences, locations=locations)
@app.route('/select')
def select():
    return render_template('select.html')


if __name__ == '__main__':
    app.run(debug=True)
