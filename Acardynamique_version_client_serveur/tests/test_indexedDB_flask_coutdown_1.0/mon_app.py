from flask import Flask, render_template, jsonify
import sqlite3

app = Flask(__name__)

# Route pour servir le template HTML
@app.route('/')
def index():
    return render_template('countdown_flask.html')

# Route pour récupérer le temps initial depuis la base de données SQLite
@app.route('/get_initial_time')
def get_initial_time():
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    c.execute('SELECT time_remaining FROM countdown WHERE id = 1')
    initial_time = c.fetchone()[0]
    conn.close()
    return jsonify({'initial_time': initial_time})

if __name__ == "__main__":
    app.run(debug=True)
