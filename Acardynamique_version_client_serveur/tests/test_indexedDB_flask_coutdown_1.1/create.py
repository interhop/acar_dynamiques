import sqlite3

# Connexion à la base de données
conn = sqlite3.connect('database.db')
c = conn.cursor()

# Création de la table countdown si elle n'existe pas déjà
c.execute('''CREATE TABLE IF NOT EXISTS countdown
             (id INTEGER PRIMARY KEY, time_remaining INTEGER)''')

# Insertion de données de démonstration
c.execute("INSERT INTO countdown (id, time_remaining) VALUES (4, 200)")  # Exemple avec 60 secondes de temps restant initial

# Commit des changements et fermeture de la connexion
conn.commit()
conn.close()
