CREATE TABLE "Acar" (
	"id"	INTEGER NOT NULL UNIQUE,
	"label"	TEXT NOT NULL,
	"short_label"	TEXT NOT NULL,
	"birth_date"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "Care_giver" (
	"id"	INTEGER NOT NULL UNIQUE,
	"surname"	TEXT NOT NULL,
	"name"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "Patient" (
	"id"	INTEGER NOT NULL UNIQUE,
	"surename"	TEXT NOT NULL,
	"name"	TEXT NOT NULL,
	"birth_date"	TEXT NOT NULL,
	"weight"	REAL NOT NULL,
	"height"	REAL NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "Slide" (
	"id"	INTEGER NOT NULL UNIQUE,
	"label"	TEXT NOT NULL,
	"short_label"	TEXT NOT NULL,
	"id_acar"	INTEGER,
	FOREIGN KEY("id_acar") REFERENCES "Acar"("id"),
	PRIMARY KEY("id" AUTOINCREMENT)
);


CREATE TABLE "Task" (
	"id"	INTEGER NOT NULL UNIQUE,
	"label"	TEXT NOT NULL,
	"short_label"	NUMERIC NOT NULL,
	"button_type"	TEXT NOT NULL,
	"button_style"	TEXT NOT NULL,
	"countdown_time"	REAL,
	"countdown"	INTEGER NOT NULL,
	"timer"	INTEGER NOT NULL,
	"timer_repeat"	INTEGER,
	"id_slide"	INTEGER NOT NULL,
	FOREIGN KEY("id_slide") REFERENCES "Slide"("id"),
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "Link_patient_caregiver_acar" (
	"id"	INTEGER NOT NULL UNIQUE,
	"id_patient"	INTEGER NOT NULL,
	"id_care_giver"	INTEGER NOT NULL,
	"id_acar"	INTEGER NOT NULL,
	"general_timer"	TEXT,
	FOREIGN KEY("id_care_giver") REFERENCES "Care_giver"("id"),
	FOREIGN KEY("id_patient") REFERENCES "Patient"("id"),
	FOREIGN KEY("id_acar") REFERENCES "Acar"("id"),
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "Log_acar" (
	"id"	INTEGER NOT NULL UNIQUE,
	"id_link_patient_caregiver_acar"	INTEGER,
	"id_task"	INTEGER,
	"done"	INTEGER,
	"datetime_done"	TEXT,
	"finished_countdown"	INTEGER,
	"timer_repeat_left"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);


CREATE TABLE "Slide_decisions" (
	"id"	INTEGER NOT NULL UNIQUE,
	"id_slide"	INTEGER,
	"task_list"	TEXT,
	"id_slide_destination"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("id_slide") REFERENCES "Slide"("id")
);